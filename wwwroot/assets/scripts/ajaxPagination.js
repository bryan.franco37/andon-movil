﻿var paginate = (function () {
    var c;
    var r;
    var cB;
    var s;

    var that = {};
    var search = "";
    var timeout = null;

    that.load = function (formatUrl) {
        mApp.block(cB);
        if (formatUrl) {
            c.load(formatUrl(r), function() { mApp.unblock(cB); });
        } else {
            c.load(r, function () { mApp.unblock(cB); });

        }
    };

    that.deserialize = function (str, options) {
        var pairs = str.split(/&amp;|&/i),
            h = {},
            options = options || {};
        for (var i = 0; i < pairs.length; i++) {
            var kv = pairs[i].split('=');
            kv[0] = decodeURIComponent(kv[0]);
            if (!options.except || options.except.indexOf(kv[0]) == -1) {
                if ((/^\w+\[\w+\]$/).test(kv[0])) {
                    var matches = kv[0].match(/^(\w+)\[(\w+)\]$/);
                    if (typeof h[matches[1]] === 'undefined') {
                        h[matches[1]] = {};
                    }
                    h[matches[1]][matches[2]] = decodeURIComponent(kv[1]);
                } else {
                    h[kv[0]] = decodeURIComponent(kv[1]);
                }
            }
        }
        return h;
    };


    that.init = function (table, routeTable, contentBlockUI, search) {
        c = $(table);
        r = routeTable;
        cB = contentBlockUI ? $(contentBlockUI) : $('.tab-content');
        s = $(search);
        if (window.location.href.indexOf("page") > -1) {
            var params = that.deserialize(window.location.href.split('#')[1]);
            var sort = "";
            if (typeof myVar !== 'undefined') {
                sort = params['sort'];
            }
            r += "?page=" + params['page'] + '&sort=' + sort;

        }
        that.load();
        table.on("click", ".page-link", function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            if (url != "javascript:;") {
                mApp.block(cB, { state: "danger" });
                search = "";
                s.val("");
                table.load(url, function () { mApp.unblock(cB); });
                var params = that.deserialize(url.split('?')[1]);
                window.history.pushState("", "", '#page=' + params['page']);
            }
        });
        table.on("click", ".page-sort", function (e) {
            e.preventDefault();
            var url = $(this).attr("href");
            if (url !== "javascript:;") {
                mApp.block(cB, { state: "danger" });
                search = "";
                s.val("");
                table.load(url, function () { mApp.unblock(cB); });
            }
        });
        s.keyup(function (e) {
            if (search !== $(this).val()) {
                clearTimeout(timeout);
                search = $(this).val();
                var url = r + '?search=' + $(this).val();
                mApp.block(cB, { state: "danger" });
                timeout = setTimeout(function () {
                    table.load(url, function () { mApp.unblock(cB); });
                }, 1000);
            }
        });
    };

    return that;
})();