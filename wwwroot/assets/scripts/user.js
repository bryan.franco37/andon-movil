﻿var colors = [
    "m-badge--success", "m-badge--warning", "m-badge--danger",
    "m-badge--info", "m-badge--primary", "m-badge--secondary", "m-badge--brand",
    "m-badge--accent", "m-badge--focus", " m-badge--metal"
];

var getColor = function (name) {
    var code = name.split("").reduce(function (a, b) {
        return a + b.charCodeAt(0);
    },
        0);
    return colors[code % colors.length];
}

var dtoToUser = function (users) {
    return users.map(function (user) {
        var initials = user.name.split(" ");
        user.Initials = initials[0].charAt(0) + ((initials[1])? initials[1].charAt(0):initials[0].charAt(1));
        user.Color = getColor(user.name);
        return user;
    });
}
$(document).ready(function () {
    var timer, cacheSearch, cacheUsers = [], userSelected = [];

    var sideBar = $("#m_quick_sidebar").mOffcanvas();
    var search = $('#search');

    var userselectedTemplate = Handlebars.compile([
        '<div class="m-widget4__item" data-user="{{email}}">',
        '    <div class="m-widget4__img m-widget4__img--pic">',
        '        <span class="m-badge {{Color}} rounded-circle" style="width: 40px; height: 40px">',
        '            <h1 style="margin-top: 8px;font-size:20px">',
        '                {{Initials}}',
        '            </h1>',
        '        </span>',
        '    </div>',
        '    <div class="m-widget4__info">',
        '        <span class="m-widget4__title">',
        '            {{name}}',
        '        </span>',
        '     <button type="button" class="close" aria-label="Close">',
        '       <span aria-hidden="true">&times;</span>',
        '      </button>',
        '        <br>',
        '        <span class="m-widget4__sub">',
        '            {{description}}',
        '        </span>',
        '    </div>',
        '</div>'
    ].join('\n'));


    var dataLoaded = false;


    var getUsers = function (search, cb) {
        var customUrl = $("#m_quick_sidebar").data("url");
        var url = (customUrl) ? customUrl : "/users/find";
        $.ajax({
            url: url,
            data: {
                search: search
            },
            complete: function () {
                mApp.unblock("#userContainer");
            },
            success: function (data) {
                dataLoaded = true;
                var users = dtoToUser(data);
                cacheUsers = users;
                cb(users);
            },
            error: console.log
        });
    };

    var getUsersInGroup = function (groupId) {
        mApp.block($("#userSelectedList"));

        $.ajax({
            url: "/users/ingroup",
            data: {
                id: groupId
            },
            complete: function() {
                mApp.unblock($("#userSelectedList"));
            },
            success: function(data) {
                var users = dtoToUser(data);
                var selectedEmails = userSelected.map(function (x) { return x.email });
                users = users.filter(function(user) {
                    return selectedEmails.indexOf(user.email) === -1;
                });
                userSelected = userSelected.concat(users);
                var userHtml = users.map(userselectedTemplate);
                $("#userSelectedList").append(userHtml);
            }
        });
    }

    var searcher = function (q, cb, async) {
        clearTimeout(timer);
        var search = q.trim();
        if (search.length === 0) {
            cb([]);
            window.setTimeout(function () {
                async([]);
            },
                100);
        }
        if (!search.startsWith(cacheSearch)) {
            dataLoaded = false;
        }
        if (cacheUsers.length < 100 && dataLoaded) {
            var filterUsers = cacheUsers.filter(function (user) {
                return user.name.toLowerCase().indexOf(search.toLowerCase()) !== -1;
            }).filter(function (obj, pos, arr) {
                return arr.map(function (mapObj) { return mapObj.email }).indexOf(obj.email) === pos;
            });
            window.setTimeout(function () {
                async([]);
            },
                100);
            return cb(filterUsers);
        }
        cacheSearch = search;
        cb([]);
        timer = window.setTimeout(function () {
            getUsers(search, async);
        },
            500);

    };

    search.typeahead({
        minLength: 2,
        classNames: {
            menu: 'tt-menu m-widget4 rounded'
        }
    },
        {
            name: 'users',
            display: 'Name',
            source: searcher,
            templates: {
                pending:
                '<div class="m-loader m-loader--danger" style="width: 30px; display: inline - block;"></div>',
                notFound: [
                    '<div class="m-widget4__item">',
                    '    <div class="m-widget4__img m-widget4__img--pic">',
                    '        <span class="m-badge m-badge--danger rounded-circle" style="width: 40px; height: 40px">',
                    '           <span class="fa fa-user-times" style="margin-top: 10px; font-size: 20px"></span>',
                    '        </span>',
                    '    </div>',
                    '    <div class="m-widget4__info">',
                    '        <span class="m-widget4__title">',
                    '            Not Match',
                    '        </span>',
                    '        <br>',
                    '        <span class="m-widget4__sub">',
                    '        </span>',
                    '    </div>',
                    '</div>'
                ].join('\n'),

                suggestion: Handlebars.compile([
                    '<div class="m-widget4__item">',
                    '    <div class="m-widget4__img m-widget4__img--pic">',
                    '        <span class="m-badge {{Color}} rounded-circle" style="width: 40px; height: 40px">',
                    '            <h1 style="margin-top: 6px;font-size:25px">',
                    '                {{Initials}} ',
                    '            </h1>',
                    '        </span>',
                    '    </div>',
                    '    <div class="m-widget4__info">',
                    '        <span class="m-widget4__title">',
                    '            {{name}}',
                    '        </span>',
                    '        <br>',
                    '        <span class="m-widget4__sub">',
                    '           {{#if isGroup}}Group {{else}}{{description}} {{/if}}',
                    '        </span>',
                    '    </div>',
                    '</div>'
                ].join('\n'))
            }
        }).on('typeahead:selected',
        function (evt, data) {
            search.typeahead('val', '');
            console.log(data);
            if (data.isGroup) {
                getUsersInGroup(data.id);
            }
            else if (userSelected.filter(function (u) { return u.email === data.email }).length === 0) {
                userSelected.push(data);
                $("#userSelectedList").append(userselectedTemplate(data));


            }

            return false;
        });

    $("#userSelectedList").on("click",
        ".close",
        function () {
            var $el = $(this).closest('.m-widget4__item');
            var user = $el.data('user');
            $el.remove();
            userSelected = userSelected.filter(function (c) {
                return c.email !== user;
            });
        });
    sideBar.on("afterHide",
        function () {
            search.typeahead('val', '');
            userSelected = [];
            $("#userSelectedList").html("");
        });

    $("#saveUsers").click(function () {
        $("#m_quick_sidebar").trigger("users:selected", [userSelected]);
        sideBar.hide();
    });


});