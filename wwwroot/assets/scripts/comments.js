﻿
(function($) {
   
    var commentRenderer = Handlebars.compile($("#commentTemplate").html());
    var loadButton = $("#loadMore");
    var commentContainer = $("#commentContainer");
    var commentSideBar;
    Handlebars.registerHelper("getInitials", function (user) {
        var initials = user.split(" ");
        return initials[0].charAt(0) + ((initials[1]) ? initials[1].charAt(0) : initials[0].charAt(1));
    });
    Handlebars.registerHelper("getColor", function (user) {
        return getColor(user);
    });
    Handlebars.registerHelper("formatDate", function (datetime, format) {
        return moment(datetime).format(format);
    });

    var init = function () {
        commentSideBar = $("#comment_sidebar").mOffcanvas({
            class: 'm-quick-sidebar',
            overlay: true,
            close: $("#comment_sidebar_close")
        });
        var topbarAsideTabs = $('#m_quick_sidebar_tabs');
        loadButton.hide();
        var resize = function () {
            var messenger = $('#m_quick_sidebar_tabs_messenger');
            var messengerMessages = messenger.find('.m-messenger__messages');

            var height = commentSideBar.outerHeight(true) -
                topbarAsideTabs.outerHeight(true) -
                messenger.find('.m-messenger__form').outerHeight(true) - 120;

            // init messages scrollable content
            messengerMessages.css('height', height);
            mApp.initScroller(messengerMessages, {});
        }

        resize();

        // reinit on window resize
        mUtil.addResizeHandler(resize);
    };

    init();

    loadButton.click(function () {
        loadComments(loadButton.data("url"), loadButton.data("nextPage"));
    });

    $.fn.tegraComments = function (options) {

        var defaults = {
            triggerSelector: ".comment-trigger",
            dataUrlSelector: function($element) {
                return $element.attr("href");
            }
        };
        var self = this;
        $("#createComment").click(function (e) {
            mApp.block(commentContainer);
            var self = this;
            mApp.block(self);
            e.preventDefault();
            var url = loadButton.data("url");
            $.ajax({
                url: url,
                method: "POST",
                data: {
                    text: $("#comment").val()
                },
                success: function (data) {
                    var html = commentRenderer({ items: [data] });
                    commentContainer.prepend(html);
                    $("#comment").val("");

                },
                complete: function () {
                    mApp.unblock(commentContainer);
                    mApp.unblock(self);
                }
            });
        });

        commentContainer.on("click", ".close", function () {

            var message = $(this).closest(".m-messenger__message");
            mApp.block(message);
            self.trigger("comment.deleted", [$(this).data("comment"), function() {
                message.remove();
            }]);
        });
        var config = $.extend({}, defaults, options);

        this.on("click",
            config.triggerSelector,
            function(e) {
                e.preventDefault();
                var $this = $(this);
                var url = config.dataUrlSelector($this);
                commentContainer.html("");
                commentSideBar.show();

                loadComments(url,1);
            });

        return this;

    };

    function loadComments(url, page) {
        mApp.block(commentContainer);
        $.ajax({
            url: url,
            data: {
                page: page
            },
            success: function (data) {
                var html = commentRenderer(data);
                loadButton.data("url", url);
                if (data.metadata.hasMorePages) {
                    loadButton.data("nextPage", data.metadata.nextPage);
                    loadButton.show();

                } else {
                    loadButton.hide();

                }
                commentContainer.append(html);

            },
            complete: function () {
                mApp.unblock(commentContainer);

            }
        });

    }

    
})(jQuery);