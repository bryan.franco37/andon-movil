﻿using I3Teck.AndonAlarm.Data.Models;

namespace I3Teck.AndonAlarm.Dto
{
    public class ProductionLineDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Operator { get; set; }
        public AndonAlert Alert { get; set; }
        public string Building { get; set; }

    }
}
