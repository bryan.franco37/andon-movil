﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Dto
{
    public class AlertDto
    {
        public int ProductionLineId { get; set; }
        public int TypeId { get; set; }
        public string Comment { get; set; }
        public int FactoryId { get; set; }
        public int ProcessId { get; set; }

    }
}
