﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Dto
{
    public class AndonAlertReportDto
    {
        public string Line { get; set; }
        public string AlertType { get; set; }
        public string Factory { get; set; }
        public string Process { get; set; }
        public string Comment { get; set; }
        public string CreatedBy { get; set; }
        public string AlertedTime { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? DeletedAt { get; set; }
        public string DeletedBy { get; set; }


    }
}
