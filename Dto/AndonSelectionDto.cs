﻿using I3Teck.AndonAlarm.Data.Models;
using System;
using System.Collections.Generic;

namespace I3Teck.AndonAlarm.Dto
{
    public class AndonSelectionDto
    {
        public AndonSelectionDto()
        {
            Factories = new List<Factory>();
            Processes = new List<Process>();
        }
        public List<Factory> Factories { get; set; }

        public List<Process> Processes { get; set; }
    }
}
