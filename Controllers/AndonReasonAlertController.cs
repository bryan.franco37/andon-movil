﻿using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;
using InternalRequisition.Repository.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace InternalRequisition.Controllers
{
    public class AndonReasonAlertController : Controller
    {
        private readonly IAndonReasonAlertRepository _andonReasonAlertRepository;

        public AndonReasonAlertController(IAndonReasonAlertRepository andonReasonAlertRepository)
        {
            _andonReasonAlertRepository = andonReasonAlertRepository;
        }

        public async Task<IActionResult> Index()
        {
            var model = await _andonReasonAlertRepository.GetAll();
            return View(model);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(AndonAlertReason reasonAlert)
        {
            if (ModelState.IsValid)
            {
                await _andonReasonAlertRepository.Create(reasonAlert);
                return RedirectToAction(nameof(Index));
            }
            return BadRequest();
        }

        public async Task<IActionResult> Edit(int id)
        {
            if (id == null)
                return NotFound();

            var reasonAlert = await _andonReasonAlertRepository.GetById(id);
            if (reasonAlert == null)
                return NotFound();

            return View(reasonAlert);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(AndonAlertReason reasonAlert)
        {
            if (ModelState.IsValid)
            {
                await _andonReasonAlertRepository.Update(reasonAlert);
                return RedirectToAction(nameof(Index));
            }
            return BadRequest();
        }

        public async Task<IActionResult> DeleteReason(int id)
        {
            if (id==0)
            {
                return NotFound();
            }

            var reasonAlert = await _andonReasonAlertRepository.GetById(id);
            if (reasonAlert==null)
            {
                return NotFound();
            }

           await _andonReasonAlertRepository.Remove(reasonAlert);
           return Ok();
        }
    }

}