﻿using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Extensions;
using I3Teck.AndonAlarm.ViewModels;
using InternalRequisition.Services.Andon.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InternalRequisition.Controllers
{
    [Authorize]
    public class AndonReportsController : Controller
    {
        private readonly IAndonReportService _andonReportService;
        private ApplicationDbContext _applicationDbContext;

        public AndonReportsController(IAndonReportService andonReportService, ApplicationDbContext applicationDbContext)
        {
            _andonReportService = andonReportService;
            _applicationDbContext = applicationDbContext;
        }


        public async Task<IActionResult> Index()
        {
            AndonReportsViewModel model = new AndonReportsViewModel
            {
                FactoryList = await _andonReportService.GetFactoyListByUser(User.Identity.GetId()),
                ProcessList = await _andonReportService.GetProcessListByUser(User.Identity.GetId()),
                WeekList = await _andonReportService.GetWeekList()
            };

            return View(model);
        }


        public async Task<IActionResult> ReportAlertTable(int factory, int process, int week, int page = 1, string sort = "", string search = "")
        {
            var details= await _andonReportService.GetAlertsPaginatedByProcessAndWeek(factory, process, week, page, sort, search);
            return View(details);
        }

        public async Task<IActionResult> ReportAlertDetailsCharts(int factoryId, int processId, int weekId)
        {
            var details = await _andonReportService.GetAlertListByProcessAndWeek(factoryId, processId, weekId);
            return Ok(details);
        }

        public async Task<IActionResult> ExcelReportByWeek(int factoryId, int processId, int weekId)
        {
            var file= await _andonReportService.GenerateExcelReport(factoryId, processId, weekId);
            return File(file, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "AndonAlerReport.xlsx");
        }

        public IActionResult Dashboard()
        {
            return View();
        }
    }
};