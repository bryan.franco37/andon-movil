﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.Extensions;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using InternalRequisition.Hubs;
using InternalRequisition.Repository.Interfaces;
using InternalRequisition.Services.Andon.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;

namespace InternalRequisition.Controllers
{
   // [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class AndonController : Controller
    {
        private readonly ApplicationDbContext _mainContext;
        private readonly IHubContext<AndonHub> _hubContext;
        private readonly UserManager<User> _userManager;
        private readonly IAndonService _andonService;
        private readonly IAndonRepository _andonRepository;
        private readonly IAndonReasonAlertRepository _andonReasonAlertRepository;
        private readonly IFactoryRepository _factoryRepository;

        public AndonController(ApplicationDbContext mainContext, IHubContext<AndonHub> hubContext, UserManager<User> userManager, IAndonService andonService, IAndonRepository andonRepository, IAndonReasonAlertRepository andonReasonAlertRepository, IFactoryRepository factoryRepository)
        {
            _mainContext = mainContext;
            _hubContext = hubContext;
            _userManager = userManager;
            _andonService = andonService;
            _andonRepository = andonRepository;
            _andonReasonAlertRepository = andonReasonAlertRepository;
            _factoryRepository = factoryRepository;
        }
        
        [HttpGet]
        public async Task<IActionResult> GetFactoriesAndProcessesMovil()
        {
            var andonSelection = await _andonService.GetFactoriesAndProcessesByUser("");
            return Ok(andonSelection);
        }

        [HttpGet]
        public async Task<IActionResult> GetReasonAlertsMovil()
        {
            var reasonAlerts = await _andonReasonAlertRepository.GetAll();
            return Ok(reasonAlerts);
        }

        [HttpGet]
        public async Task<List<ProductionLineDto>> ProductionLinesByProcessAndFactoryMovil(int FactoryId, int ProcessId)
        {
            //var userId = User.Identity.GetId();
            //var isSupervisor = User.IsInRole("ProductionLineSupervisor");
            //var isCoordinator = User.IsInRole("ProductionLineCoordinator");
            //var isAndonAdministrador = User.IsInRole("AndonAdministrator") || User.IsInRole("Administrator");
            //if (isAndonAdministrador)
            //{
                return await _andonService.GetProductionLines(FactoryId, ProcessId);

            //}
            //return await _andonService.GetProductionLinesByAlias(userId, FactoryId, ProcessId, isSupervisor);
        }

        [HttpGet]
        public async Task<List<ProductionLineDto>> ProductionLineByFactory(int factoryId)
        {
            //var factory = await _factoryRepository.GetFactoryById(factoryId);
            //return await _andonRepository.GetProductionLineByFactory(factory.Prefix);
            throw new NotImplementedException();
        }

        [HttpPost]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> CreateAlertMovil([FromBody] AlertDto alertDto)
        {
            var andonAlert =
                await _andonRepository.GetAlertByPpmAddress(alertDto.ProductionLineId) ;
            if (andonAlert != null && (andonAlert.AndonAlertTypeId == 1 ||
                                       (andonAlert.AndonAlertTypeId == 2 && alertDto.TypeId == 2)))
            {
                return BadRequest(new
                {
                    message = "The production Line already has an alert",
                    error = true
                });
            }

            if (andonAlert != null)
                andonAlert.DeletedAt = DateTime.UtcNow;

            var alert = new AndonAlert
            {
                //CreatedByUserId = User.Identity.GetId(),
                AndonAlertTypeId = alertDto.TypeId,
                ProductionLineId = alertDto.ProductionLineId,
                Comment = alertDto.Comment
            };

            _mainContext.AndonAlerts.Add(alert);
            //var hasWarning = _mainContext.AndonAlerts
            //    .Where(x => x.ProductionLineId == alert.ProductionLineId && x.AndonAlertTypeId == 2)
            //    .Select(x => x.Id).LastOrDefault();
            //if (hasWarning > 0)
            //{
            //    await RemoveAlertMovil(hasWarning);
            //}

            await _mainContext.SaveChangesAsync();
            if (alertDto.FactoryId > 0 && alertDto.ProcessId > 0)
                await _hubContext.Clients.Group($"{alertDto.FactoryId}_{alertDto.ProcessId}")
                    .SendAsync("ReceiveAlert", alertDto.ProductionLineId, alert);
            else
                await _hubContext.Clients.All.SendAsync("ReceiveAlert", alertDto.ProductionLineId, alert);

            return Ok(alert);
        }

        [HttpDelete]
        //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
        public async Task<IActionResult> RemoveAlertMovil(int id)
        {

            await _andonService.CloseAndonAlert(id,"");
            var alert = await _andonRepository.GetAlertInfoById(id);

            if (alert.ProductionLine.Building.FactoryId > 0 && alert.ProductionLine.ProcessId > 0)
                await _hubContext.Clients.Group($"{alert.ProductionLine.Building.FactoryId}_{alert.ProductionLine.ProcessId}").SendAsync("RemoveAlert", alert.ProductionLineId);
            else
                await _hubContext.Clients.All.SendAsync("RemoveAlert", alert.ProductionLineId);
            return NoContent();
            
        }

        

    }
}