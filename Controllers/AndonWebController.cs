﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using InternalRequisition.Hubs;
using InternalRequisition.Repository.Interfaces;
using InternalRequisition.Services.Andon.Interfaces;
using Microsoft.AspNetCore.Authorization;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.Extensions;
using I3Teck.AndonAlarm.ViewModels;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using I3Teck.AndonAlarm.Data.Models;

namespace InternalRequisition.Controllers
{
    //[Authorize]
    public class AndonWebController : Controller
    {
        private readonly ApplicationDbContext _mainContext;
        private readonly IHubContext<AndonHub> _hubContext;
        private readonly UserManager<User> _userManager;
        private readonly IAndonService _andonService;
        private readonly IAndonRepository _andonRepository;
        private readonly IAndonReasonAlertRepository _andonReasonAlertRepository;


        public AndonWebController(ApplicationDbContext mainContext, IHubContext<AndonHub> hubContext, UserManager<User> userManager, IAndonService andonService, IAndonRepository andonRepository, IAndonReasonAlertRepository andonReasonAlertRepository)
        {
            _mainContext = mainContext;
            _hubContext = hubContext;
            _userManager = userManager;
            _andonService = andonService;
            _andonRepository = andonRepository;
            _andonReasonAlertRepository = andonReasonAlertRepository;
        }


        public IActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [Route("Andon/{Factory}/{Process}/tv/{All:int?}")]
        public async Task<IActionResult> AndonTv(string factory, string process, int all)
        {
            try
            {
                var model = await _andonService.FillAndonTvViewModel(factory, process, all);
                return View(model);
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }

        [Route("api/Andon/{FactoryId}/{ProcessId}/ProductionLinesTv")]
        [HttpGet]
        [AllowAnonymous]
        public async Task<List<ProductionLineDto>> ProductionLinesByProcessTv(int factoryId, int processId)
        {
            return await _andonService.GetProductionLines(factoryId, processId);

        }

        [AllowAnonymous]
        public async Task<IActionResult> GetFactoriesAndProcesses()
        {
            var a = await _andonService.GetFactoriesAndProcessesByUser(User.Identity.GetId()); 
           
            var model = new ManageCoordinatorViewModel { Factories = a.Factories, Processes = a.Processes };
            return Ok(model);
        }

        public async Task<List<ProductionLineDto>> ProductionLines(int factoryId, int processId)
        {
            var userId = User.Identity.GetId();
            var isSupervisor = User.IsInRole("ProductionLineSupervisor");
            var isCoordinator = User.IsInRole("ProductionLineCoordinator");
            var isAndonAdministrador = User.IsInRole("AndonAdministrator") || User.IsInRole("Administrator");
            if (isAndonAdministrador)
            {

                return await _andonService.GetProductionLines(factoryId, processId);
            }
            return await _andonService.GetProductionLinesByAlias(userId, factoryId, processId, isSupervisor);
        }

        [AllowAnonymous]
        public async Task<List<AndonAlertReason>> GetReasonAlerts()
        {
            return await _andonReasonAlertRepository.GetAll();
            
        }

        public async Task<IActionResult> CreateAlert([FromBody] AlertDto alertDto)
        {
            var andonAlert = await _andonRepository.GetAlertByPpmAddress(alertDto.ProductionLineId);
            if (andonAlert != null && (andonAlert.AndonAlertTypeId == 1 || (andonAlert.AndonAlertTypeId == 2 && alertDto.TypeId == 2)))
            {
                return BadRequest(new
                {
                    message = "The production Line already has an alert",
                    error = true
                });
            }
            if (andonAlert != null)
                andonAlert.DeletedAt = DateTime.UtcNow;


            var alert = new AndonAlert
            {
                CreatedByUserId = User.Identity.GetId(),
                AndonAlertTypeId = alertDto.TypeId,
                ProductionLineId = alertDto.ProductionLineId,
                Comment = alertDto.Comment,
            };

            _mainContext.AndonAlerts.Add(alert);
            var alerts =  await  _andonRepository.GetActiveAlerts(alertDto.FactoryId,alertDto.ProcessId);
            var hasWarning = alerts
                .Where(x => x.ProductionLineId == alert.ProductionLineId && x.AndonAlertTypeId == 2 && x.ProductionLine.Building.FactoryId == alert.ProductionLine.Building.FactoryId && x.ProductionLine.ProcessId == alert.ProductionLine.ProcessId)
                .Select(x => x.Id).LastOrDefault();
            if (hasWarning > 0)
            {
                await RemoveAlert(hasWarning);
            }
            await _mainContext.SaveChangesAsync();
            if (alertDto.FactoryId > 0 && alertDto.ProcessId > 0)
                await _hubContext.Clients.Group($"{alertDto.FactoryId}_{alertDto.ProcessId}").SendAsync("ReceiveAlert", alertDto.ProductionLineId, alert);
            else
                await _hubContext.Clients.All.SendAsync("ReceiveAlert", alertDto.ProductionLineId, alert);

            return Ok(alert);
        }

        public async Task<IActionResult> RemoveAlert(int id)
        {
             await _andonService.CloseAndonAlert(id, User.Identity.GetId());
             var alert = await _andonRepository.GetAlertInfoById(id);
             
            if (alert.ProductionLine.Building.FactoryId > 0 && alert.ProductionLine.ProcessId > 0)
                await _hubContext.Clients.Group($"{alert.ProductionLine.Building.FactoryId}_{alert.ProductionLine.ProcessId}").SendAsync("RemoveAlert", alert.ProductionLineId);
            else
                await _hubContext.Clients.All.SendAsync("RemoveAlert", alert.ProductionLineId);
            return NoContent();
        }

        public async Task<IActionResult> SetLeadTimeClosedAlerts()
        {
            await _andonService.SetLeadTimesClosedAlerts();

            return NoContent();
        }


        #region Dashboard 

        public IActionResult Dashboard()
        {
            return View("~/Views/AndonWeb/Dashboard.cshtml");
        }

        #endregion
    }
}
