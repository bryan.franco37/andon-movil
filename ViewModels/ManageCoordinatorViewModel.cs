﻿using I3Teck.AndonAlarm.Data.Models;
using System;
using System.Collections.Generic;

namespace I3Teck.AndonAlarm.ViewModels
{
    public class ManageCoordinatorViewModel
    {
        public ManageCoordinatorViewModel()
        {
            Factories = new List<Factory>();
            Processes = new List<Process>();
            Coordinators = new List<string>();       
        }
        
        public virtual Factory  Factory { get; set; }

        public List<Factory> Factories { get; set; }
        
        public virtual Process Process { get; set; }

        public List<Process> Processes { get; set; }

        public List<string> Coordinators { get; set; }

        public virtual string NewCoordinator { get; set; }





    }
}
