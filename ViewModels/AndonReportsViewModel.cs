﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using I3Teck.AndonAlarm.Dto;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace I3Teck.AndonAlarm.ViewModels
{
    public class AndonReportsViewModel
    {
        [Required]
        [Display(Name = "Factory")]
        public int Factory { get; set; }
        public List<SelectListItem> FactoryList { get; set; }

        [Required]
        [Display(Name = "Process")]
        public int Process { get; set; }
        public List<SelectListItem> ProcessList { get; set; }

        [Required]
        [Display(Name = "Week")]
        public int Week { get; set; }
        public List<SelectListItem> WeekList { get; set; }

        public List<AndonAlertReportDto> AlertReportDetails { get; set; }
        public int TotalLines { get; set; }


    }
}
