﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.ViewModels
{
    public class AndonTvViewModel
    {
        public int FactoryId { get; set; }

        public string Factory { get; set; }

        public string Process { get; set; }

        public int ProcessId { get; set; }

        public int ShowAllLines { get; set; }

    }
}
