﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Enum
{
    public enum ClientRolesEnum
    {
        Admin = 1,
        User = 2,
        Viewer = 3
    }

    public enum UserClaimsTypeEnum
    {
        FactoryId, 
        Planning,
        Assessment,
        ApprovalFlowId,
    }
}
