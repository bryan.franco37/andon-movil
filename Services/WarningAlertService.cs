﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternalRequisition.Services.Andon.Interfaces;
using InternalRequisition.Services.Hosted;
using Microsoft.Extensions.DependencyInjection;

namespace InternalRequisition.Services.Andon
{
    public class WarningAlertService : ScheduleBackgroundService
    {
        public WarningAlertService(IServiceScopeFactory serviceScopeFactory) : base(serviceScopeFactory)
        {
        }

        protected override string Schedule => "*/5 * * * *";
        public override async Task ProcessInScope(IServiceProvider serviceProvider)
        {
            var alertService = serviceProvider.GetService<IAndonService>();

            await alertService.CheckWarningAlerts();
        }
    }
}
