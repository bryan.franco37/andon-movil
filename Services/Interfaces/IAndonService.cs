﻿using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.ViewModels;

namespace InternalRequisition.Services.Andon.Interfaces
{
    public interface IAndonService
    {
        Task<AndonTvViewModel> FillAndonTvViewModel(string factory, string process, int alert = 0);
        Task<AndonSelectionDto> GetFactoriesAndProcessesByUser(string userId);
        Task<List<ProductionLineDto>> GetProductionLines(int factoryId, int processId);

        Task<List<ProductionLineDto>> GetProductionLinesByAlias(string userId, int factoryId, int processId,
            bool isSupervisor);

        Task<ProductionLineDto> GetProductionLineTablet(string factoryName, string processName, int ppmAddressId);

        Task<bool> CloseAndonAlert(int andonAlertId, string userId);

        Task<bool> SetLeadTimesClosedAlerts();

        Task CheckWarningAlerts();
    }
}