﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Collection;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.ViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace InternalRequisition.Services.Andon.Interfaces
{
    public interface IAndonReportService
    {
        Task<List<SelectListItem>> GetFactoyListByUser(string userId);
        Task<List<SelectListItem>> GetProcessListByUser(string userId);
        Task<List<SelectListItem>> GetWeekList();
        Task<PagedList<AndonAlertReportDto>> GetAlertsPaginatedByProcessAndWeek(int factoryId, int processId, int weekId,int page, string sort, string search);
        Task<AndonReportsViewModel> GetAlertListByProcessAndWeek(int factoryId, int processId, int weekId);
        Task<Byte[]> GenerateExcelReport(int factoryId, int processId, int weekId);
    }
}