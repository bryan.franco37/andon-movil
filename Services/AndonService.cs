﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternalRequisition.Hubs;
using InternalRequisition.Services.Andon.Interfaces;
using Microsoft.AspNetCore.SignalR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.ViewModels;
using I3Teck.AndonAlarm.Data;

namespace InternalRequisition.Services.Andon
{
    public class AndonService : IAndonService
    {
        private readonly IFactoryRepository _factoryRepository;
        private readonly IProcessRepository _processRepository;
        private readonly IAndonRepository _andonRepository;
        private readonly IUserRepository _userRepository;
        private readonly IShiftRepository _shiftRepository;
        private readonly IScheduleRepository _scheduleRepository;
        private readonly IHubContext<AndonHub> _hubContext;
        private readonly ILogger<AndonService> _logger;
        private readonly ApplicationDbContext _context;

        public AndonService(IProcessRepository processRepository, IFactoryRepository factoryRepository,
            IAndonRepository andonRepository, IHubContext<AndonHub> hubContext, ILogger<AndonService> logger,
            IUserRepository userClaimsRepository, IShiftRepository shiftRepository, IScheduleRepository scheduleRepository,
            ApplicationDbContext context
            )
        {
            _processRepository = processRepository;
            _factoryRepository = factoryRepository;
            _andonRepository = andonRepository;
            _hubContext = hubContext;
            _logger = logger;
            _userRepository = userClaimsRepository;
            _shiftRepository = shiftRepository;
            _scheduleRepository = scheduleRepository;
            _context = context;
        }

        public async Task<AndonTvViewModel> FillAndonTvViewModel(string factory, string process, int alert = 0)
        {
            AndonTvViewModel model = new AndonTvViewModel {Factory = factory, Process = process, ShowAllLines = alert};
            try
            {
                var Factory = await _factoryRepository.GetFactoryByName(factory);
                var Process = await _processRepository.GetProcessByName(process);
                model.FactoryId = Factory.Id;
                model.ProcessId = Process.Id;
            }
            catch (Exception ex)
            {
                _logger.LogCritical(ex, "Get Orders thrown an error");
                throw;
            }

            return model;
        }

        public async Task<AndonSelectionDto> GetFactoriesAndProcessesByUser(string userId)
        {
            //var userClaims = await _userRepository.GetClaimsByUserId(userId);
            var factories = await _factoryRepository.GetAllFactories();
            var processes = await _processRepository.GetAllProcesses();
           // var factoriesId = userClaims.Where(x => x.ClaimType.Contains("FactoryId"))
               // .Select(x => int.Parse(x.ClaimValue)).ToList();
           // var processesId = userClaims.Where(x => x.ClaimType.Contains("Planning")).Select(x => x.ClaimValue)
//.ToList();
            var andonSelection = new AndonSelectionDto();
            andonSelection.Factories = factories;
            andonSelection.Processes = processes;
            return andonSelection;
        }

        public async Task<List<ProductionLineDto>> GetProductionLines(int factoryId, int processId)
        {
            var alerts = await _andonRepository.GetActiveAlerts(factoryId, processId);
            return await _andonRepository.GetProductionLinesByProcessAndFactory(alerts,factoryId,processId);
        }

        public async Task<List<ProductionLineDto>> GetProductionLinesByAlias(string userId, int factoryId,
            int processId, bool isSupervisor)
        {
            var alerts = await _andonRepository.GetActiveAlerts(factoryId, processId);
            
            return  _context.UserProductionLines.Include("ProductionLine").Include("ProductionLine.Building").Where(x => x.UserId == userId &&x.ProductionLine.Building.FactoryId == factoryId 
            && x.ProductionLine.ProcessId == processId).ToList().Select(x =>
                                                       new ProductionLineDto
                                                       {
                                                           Id = x.ProductionLineId,
                                                           Name = x.ProductionLine.Name,
                                                           Building = x.ProductionLine.Building.Name,
                                                           Alert= alerts.FirstOrDefault(y => y.ProductionLineId == x.ProductionLineId),
                                                           
                                                       }).OrderBy(x => x.Building + x.Name).ToList(); 
            

        }


        public async Task<ProductionLineDto> GetProductionLineTablet(string factoryName, string processName,
            int ppmAddressId)
        {
            //var factory = await _factoryRepository.GetFactoryByName(factoryName);
            //var process = await _processRepository.GetProcessByName(processName);
            //var alerts = await _andonRepository.GetActiveAlerts(factory.Id, process.ProcessId);
            //var productionLine =
            //    await _andonRepository.GetProductionLineByPpmAddresId(alerts, factory.Prefix, process.ContactTitle,
            //        ppmAddressId);
            //return productionLine;
            throw new NotImplementedException();
        }

        public async Task CheckWarningAlerts()
        {
            var warnings = await _andonRepository.GetWarningAlerts();
            if (warnings.Count >0)
            {
                var warningToClose = warnings.Where(x => DateTime.UtcNow.Subtract(x.CreatedAt).Hours >= 2).ToList();

                if (warningToClose.Count>0)
                {
                    var cronUser = await _userRepository.GetAndonCron();
                    warningToClose.ForEach(x =>
                    {
                        x.DeletedAt = DateTime.UtcNow;
                        x.DeletedByUserId = cronUser.Id;
                    });
                    _andonRepository.RemoveAlertRange(warningToClose);
                    await _andonRepository.Commit();

                    foreach (var alert in warningToClose)
                    {
                        await _hubContext.Clients.Group($"{alert.ProductionLine.Building.FactoryId}_{alert.ProductionLine.ProcessId}").SendAsync("RemoveAlert", alert.ProductionLineId);

                    }

                    var alertLines = warningToClose.Select(x => new AndonAlert
                    {
                        CreatedByUserId = cronUser.Id,
                        AndonAlertTypeId = 1,
                        ProductionLineId = x.ProductionLineId,
                        Comment = x.Comment,
                    }).ToList();

                    await _andonRepository.CreateAlertRange(alertLines);
                    await _andonRepository.Commit();

                    foreach (var alert in alertLines)
                    {
                        //@todo fix this eager loading
                        await _hubContext.Clients.Group($"{alert.ProductionLine.Building.FactoryId}_{alert.ProductionLine.ProcessId}").SendAsync("ReceiveAlert", alert.ProductionLineId, alert);

                    }
                }
            }
            
        }

        public async Task<bool> CloseAndonAlert(int andonAlertId, string userId)
        {
            try
            {
                await _andonRepository.DeleteAndonAlert(andonAlertId, userId);
                await _andonRepository.Commit();

                //TODO: Cuando las alertas andon se registren por turno se debe guardar el turno al momento de crear la alerta y obtenerla para el momento del cierre. 
                var alert = await _andonRepository.GetAlertInfoById(andonAlertId);
                var shift = await _shiftRepository.GetShiftByIdAsync(1);
                var schedule = await _scheduleRepository.GetScheduleByFactoryAsync(alert.ProductionLine?.Building.FactoryId ?? 1, shift.Id);

                if (schedule == null)
                    return false;

                var scheduleD = schedule.Select(s => new {s.DayOfWeek, s.StartTime, s.EndTime  })
                    .ToDictionary(d => d.DayOfWeek, d=> new {d.StartTime, d.EndTime});

               var start = alert.CreatedAt;
                var aux = new DateTime(start.Ticks);
                var end = (alert.DeletedAt?? new DateTime());
                double realLeadtime = 0;
                
                TimeSpan li, lf;
                while (aux.DayOfYear<=end.DayOfYear)
                {
                    if (scheduleD.ContainsKey((int) aux.DayOfWeek))
                    {
                        li = (aux.DayOfYear == start.DayOfYear && start.TimeOfDay >= scheduleD[(int)aux.DayOfWeek].StartTime) ? start.TimeOfDay : scheduleD[(int)aux.DayOfWeek].StartTime;
                        lf = (aux.DayOfYear == end.DayOfYear && end.TimeOfDay <= scheduleD[(int)aux.DayOfWeek].EndTime) ? end.TimeOfDay : scheduleD[(int)aux.DayOfWeek].EndTime;
                        realLeadtime += (lf - li).TotalMinutes;
                    }
                    aux = aux.AddDays(1);
                }

                await _andonRepository.SetLeadTimes(andonAlertId, (end - start).TotalMinutes, realLeadtime);
                await _andonRepository.Commit();
               
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error closing andon alert {ex.Message}");
                return false;
            }

        }

        public async Task<bool> SetLeadTimesClosedAlerts()
        {

            try
            {

                List<AndonAlert> lst = await _andonRepository.GetClosedAndonAlerts();

                foreach (var alert in lst)
                {

                    //TODO: Cuando las alertas andon se registren por turno se debe guardar el turno al momento de crear la alerta y obtenerla para el momento del cierre. 
                    var shift = await _shiftRepository.GetShiftByIdAsync(1);
                    var schedule = await _scheduleRepository.GetScheduleByFactoryAsync((alert.ProductionLine?.Building.FactoryId ?? 1), shift.Id);

                    if (schedule == null)
                        return false;

                    var scheduleD = schedule.Select(s => new { s.DayOfWeek, s.StartTime, s.EndTime })
                        .ToDictionary(d => d.DayOfWeek, d => new { d.StartTime, d.EndTime });

                    var start = alert.CreatedAt;
                    var aux = new DateTime(start.Ticks);
                    var end = (alert.DeletedAt ?? new DateTime());
                    double realLeadtime = 0;

                    TimeSpan li, lf;
                    while (aux.DayOfYear <= end.DayOfYear)
                    {
                        int dow= (int)aux.DayOfWeek;
                        if (scheduleD.ContainsKey(dow))
                        {
                            li = (aux.DayOfYear == start.DayOfYear && start.TimeOfDay >= scheduleD[(int)aux.DayOfWeek].StartTime) ? start.TimeOfDay : scheduleD[(int)aux.DayOfWeek].StartTime;
                            lf = (aux.DayOfYear == end.DayOfYear && end.TimeOfDay<= scheduleD[(int)aux.DayOfWeek].EndTime) ? end.TimeOfDay : scheduleD[(int)aux.DayOfWeek].EndTime;
                            realLeadtime += (lf - li).TotalMinutes;
                        }
                        aux = aux.AddDays(1);
                    }

                    await _andonRepository.SetLeadTimes(alert.Id, (end - start).TotalMinutes, realLeadtime);
                }

                await _andonRepository.Commit();
                return true;

            }
            catch (Exception ex)
            {
                _logger.LogError($"Error closing andon alert {ex.Message}");
                return false;
            }

        }


    }
}