﻿using System;
using System.Drawing;
using System.Linq;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.ViewModels;
using OfficeOpenXml;
using OfficeOpenXml.Style;

namespace InternalRequisition.Services.Andon.AndonReports.ReportTemplates
{
    public class AndonExcelReportTemplate
    {
        public  Byte[] GenerateExcelReport(Factory factoryId, Process processId, AndonReportsViewModel details)
        {
            byte[] fileContents;
            var factory = factoryId;
            var process = processId;
            var week = 0; //todo: get week id
            var reportDetails = details;
            using (var package = new ExcelPackage())
            {
                var worksheet = package.Workbook.Worksheets.Add(factory.Name + "_" + process.Name);
                using (ExcelRange range = worksheet.Cells[1, 1, 1, 2])
                {
                    range.Merge = true;
                    range.Value = $@"Factory: {factory.Name}";
                    range.Style.Font.Bold = true;
                }

                using (ExcelRange range = worksheet.Cells[1, 6, 1, 8])
                {
                    range.Merge = true;
                    range.Value = $@"Process: {process.Name}";
                    range.Style.Font.Bold = true;
                }

                worksheet.Cells[1, 1, 1, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, 1, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[1, 1, 1, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[1, 1, 1, 8].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen);
                worksheet.Cells[2, 1, 2, 8].Merge = true;
                //FIX THIS
                // worksheet.Cells[2, 1].Value = "Week: " + week.Week + "   " + "Range Date: " + week.StartDate.Value.ToShortDateString() + "  to " + week.EndDate.Value.ToShortDateString();
                worksheet.Cells[2, 1].Style.Font.Bold = true;
                worksheet.Row(1).Height = 25;
                worksheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(1).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Row(2).Height = 25;
                worksheet.Row(2).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(2).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                worksheet.Column(6).Width = 22;
                worksheet.Column(7).Width = 22;
                worksheet.Cells[2, 1].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[2, 1, 2, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[2, 1].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[2, 7].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[2, 1, 2, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[2, 1, 2, 8].Style.Fill.BackgroundColor.SetColor(Color.MediumSeaGreen);
                worksheet.Row(3).Height = 40;
                worksheet.Cells[3, 1].Value = "Production Line";
                worksheet.Cells[3, 1].Style.Font.Bold = true;
                worksheet.Cells[3, 2].Value = "Alert Type";
                worksheet.Cells[3, 2].Style.Font.Bold = true;
                worksheet.Cells[3, 3].Value = "Created By User";
                worksheet.Cells[3, 3].Style.Font.Bold = true;
                worksheet.Cells[3, 4].Value = "Comment";
                worksheet.Cells[3, 4].Style.Font.Bold = true;
                worksheet.Cells[3, 5].Value = "Alerted Time";
                worksheet.Cells[3, 5].Style.Font.Bold = true;
                worksheet.Cells[3, 6].Value = "From";
                worksheet.Cells[3, 6].Style.Font.Bold = true;
                worksheet.Cells[3, 7].Value = "To";
                worksheet.Cells[3, 7].Style.Font.Bold = true;
                worksheet.Cells[3, 8].Value = "Deleted By User";
                worksheet.Cells[3, 8].Style.Font.Bold = true;
                worksheet.Cells[3, 1, 3, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[3, 1, 3, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[3, 1, 3, 8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[3, 1, 3, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                worksheet.Cells[3, 1, 3, 8].Style.Fill.PatternType = ExcelFillStyle.Solid;
                worksheet.Cells[3, 1, 3, 8].Style.Fill.BackgroundColor.SetColor(Color.DodgerBlue);
                worksheet.Cells[3, 1, 3, 8].Style.Font.Color.SetColor(Color.AliceBlue);
                worksheet.Row(3).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                worksheet.Row(3).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                var rowCounter = 4;
                for (int i = 0; i < reportDetails.AlertReportDetails.Count; i++)
                {
                    worksheet.Row(rowCounter).Height = 25;
                    worksheet.Cells[rowCounter, 1, rowCounter, 8].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowCounter, 1, rowCounter, 8].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowCounter, 1, rowCounter, 8].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowCounter, 1, rowCounter, 8].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                    worksheet.Cells[rowCounter, 1].Value = reportDetails.AlertReportDetails[i].Line;
                    switch (reportDetails.AlertReportDetails[i].AlertType)
                    {
                        case "1":
                            worksheet.Cells[rowCounter, 2].Value = "Alert";
                            break;
                        case "2":
                            worksheet.Cells[rowCounter, 2].Value = "Warning";
                            break;
                        case "3":
                            worksheet.Cells[rowCounter, 2].Value = "Workless";
                            break;
                    }

                    worksheet.Cells[rowCounter, 3].Value = reportDetails.AlertReportDetails[i].CreatedBy;
                    worksheet.Cells[rowCounter, 4].Value = !string.IsNullOrEmpty(reportDetails.AlertReportDetails[i].Comment)
                        ? reportDetails.AlertReportDetails[i].Comment
                        : " - ";
                    worksheet.Cells[rowCounter, 5].Value = reportDetails.AlertReportDetails[i].AlertedTime;
                    worksheet.Cells[rowCounter, 6].Style.Numberformat.Format = "mm/dd/yyyy hh:mm:ss AM/PM";
                    worksheet.Cells[rowCounter, 6].Value = reportDetails.AlertReportDetails[i].CreatedAt.Value;
                    worksheet.Cells[rowCounter, 7].Style.Numberformat.Format = "mm/dd/yyyy hh:mm:ss AM/PM";
                    worksheet.Cells[rowCounter, 7].Value = reportDetails.AlertReportDetails[i].DeletedAt.HasValue
                        ? (object) Convert.ToDateTime(reportDetails.AlertReportDetails[i].DeletedAt)
                        : "Actually Alert";
                    worksheet.Cells[rowCounter, 8].Value = reportDetails.AlertReportDetails[i].DeletedBy;

                    worksheet.Row(rowCounter).Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                    worksheet.Row(rowCounter).Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    rowCounter++;
                }

                worksheet.Cells.AutoFitColumns();
                worksheet.Column(6).Width = 22;
                worksheet.Column(7).Width = 22;
                fileContents = package.GetAsByteArray();
                return fileContents;
            }
        }
    }
}
