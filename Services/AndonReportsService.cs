﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Collection;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.Enum;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using I3Teck.AndonAlarm.ViewModels;
using InternalRequisition.Services.Andon.AndonReports.ReportTemplates;
using InternalRequisition.Services.Andon.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InternalRequisition.Services.Andon
{
    public class AndonReportsService : IAndonReportService
    {
        private readonly ApplicationDbContext _applicationDbContext;
        private readonly IAndonRepository _andonRepository;
        private readonly IFactoryRepository _factoryRepository;
        private readonly IProcessRepository _processRepository;
        private readonly IUserRepository _userRepository;


        public AndonReportsService(ApplicationDbContext applicationDbContext,  IAndonRepository andonAlertRepository, IFactoryRepository factoryRepository, IProcessRepository processRepository, IUserRepository userRepository)
        {
            _applicationDbContext = applicationDbContext;
            _andonRepository = andonAlertRepository;
            _factoryRepository = factoryRepository;
            _processRepository = processRepository;
            _userRepository = userRepository;
        }

        public async Task<List<SelectListItem>> GetFactoyListByUser(string userId)
        {
            var user = _applicationDbContext.Users.FirstOrDefault(x => x.Id == userId);
            var userClaims = await _applicationDbContext.UserClaims
                .Where(x => x.UserId == user.Id && x.ClaimType == nameof(UserClaimsTypeEnum.FactoryId))
                .Select(x => Convert.ToInt32(x.ClaimValue)).ToListAsync();
            return await _applicationDbContext.Factories.Where(x => userClaims.Contains(x.Id))
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem {Value = x.Id.ToString(), Text = x.Name}).ToListAsync();
        }

        public async Task<List<SelectListItem>> GetProcessListByUser(string userId)
        {
            var user = _applicationDbContext.Users.FirstOrDefault(x => x.Id == userId);
            var userClaims = await _applicationDbContext.UserClaims
                .Where(x => x.UserId == user.Id && x.ClaimType == nameof(UserClaimsTypeEnum.Planning))
                .Select(x => x.ClaimValue).ToListAsync();
            return await _applicationDbContext.Processes.Where(x => userClaims.Contains(x.Name)).OrderBy(x => x.Name)
                .Select(x => new SelectListItem {Value = x.Id.ToString(), Text = x.Name}).ToListAsync();
        }

        public async Task<List<SelectListItem>> GetWeekList()
        {
            //DateTime t = DateTime.Now;
            //var cur = _tegraEdwContext.DimWeekCalendar.Where(x => x.StartDate <= t.Date && x.EndDate >= t.Date).FirstOrDefaultAsync();

            //var lst = await _tegraEdwContext
            //    .DimWeekCalendar
            //    .Where(x => (x.StartDate <= cur.Result.StartDate) && (x.EndDate <= cur.Result.EndDate))
            //    .OrderByDescending(x => x.Year)
            //    .ThenByDescending(x => x.Week)
            //    .Select(x => new SelectListItem
            //    {
            //        Value = x.Id.ToString(),
            //        Text =
            //            $"Week {x.Week} ({x.StartDate.Value:dd/MM/yyyy} - {x.EndDate.Value:dd/MM/yyyy})"
            //    }).ToListAsync();

            //foreach (var w in lst.Where(x => x.Value == cur.Result.Id.ToString())) w.Selected = true;

            //return lst;
            throw new NotImplementedException();
        }

        public async Task<PagedList<AndonAlertReportDto>> GetAlertsPaginatedByProcessAndWeek(int factoryId, int processId, int weekId, int page, string sort, string search)
        {
            //var week = await _tegraEdwContext.DimWeekCalendar.FirstOrDefaultAsync(x => x.Id == weekId);
            //var week = 1;
            //var factory = await _factoryRepository.GetFactoryById(factoryId);
            //var process = await _processRepository.GetProcessById(processId);
            //var alerts = await _andonRepository.GetAlertsByDateRange(factory.Id,process.ProcessId, week.StartDate,week.EndDate);
            //var lines = await _andonRepository.GetProductionLinesByProcessAndFactory(alerts, factory.Prefix, process.ContactTitle);
            //var users = await _userRepository.GetUsers();

            //var reportDetails = alerts.Select(x => new AndonAlertReportDto
            //{
            //    Line = lines.FirstOrDefault(y => x != null && y.Id.Equals(x.PpmAddressId))?.Name,
            //    Factory = factory.Name,
            //    Process = process.Name,
            //    AlertedTime = x.DeletedAt.HasValue
            //        ? (((DateTime)x.DeletedAt) - x.CreatedAt).ToHumanReadFormat() : (DateTime.Now- x.CreatedAt).ToHumanReadFormat(),
            //    CreatedAt = x.CreatedAt,
            //    DeletedAt = x.DeletedAt,
            //    AlertType = x.AndonAlertTypeId.ToString(),
            //    Comment = x.Comment,
            //    CreatedBy =  users.Where(y => y.Id == x.UserId).Select(y => (y.FirstName + " " + y.LastName).ToUpper())
            //        .FirstOrDefault(),
            //    DeletedBy = x.DeletedByUser.HasValue()  ? users.Where(y => y.Id == x.DeletedByUser).Select(y => (y.FirstName + " " + y.LastName).ToUpper()).FirstOrDefault() : "-"
            //}).Where(x=> (string.IsNullOrEmpty(search))  || (!string.IsNullOrEmpty(search) && ($"{x.Line.ToString()} {x.CreatedBy.ToString()} {x.AlertType.ToString()} {x.Comment.ToString()}").ToLower().Contains(search.ToLower()))).ToPagedList(page, 10, sort);
            //return reportDetails;
            throw new NotImplementedException();
        }

        public async Task<AndonReportsViewModel> GetAlertListByProcessAndWeek(int factoryId, int processId, int weekId)
        {
            //var week = await _tegraEdwContext.DimWeekCalendar.FirstOrDefaultAsync(x => x.Id == weekId);
            //var week = 0;
            //var factory = await _factoryRepository.GetFactoryById(factoryId);
            //var process = await _processRepository.GetProcessById(processId);
            //var alerts = await _andonRepository.GetAlertsByDateRange(factory.Id, process.ProcessId, week.StartDate, week.EndDate);
            //var lines = await _andonRepository.GetProductionLinesByProcessAndFactory(alerts, null, null); // puse nulls esto
            //var users = await _userRepository.GetUsers();

            //var reportDetails = alerts.Select(x => new AndonAlertReportDto
            //{
            //    Line = lines.FirstOrDefault(y => x != null && y.Id.Equals(x.PpmAddressId))?.Name,
            //    Factory = factory.Name,
            //    Process = process.Name,
            //    AlertedTime = x.DeletedAt.HasValue
            //        ? ( x.DeletedAt  - x.CreatedAt).Value.ToHumanReadFormat() : (DateTime.Now.ToLocalTime() - x.CreatedAt).ToHumanReadFormat(),
            //    CreatedAt = x.CreatedAt,
            //    DeletedAt = x.DeletedAt.HasValue ? x.DeletedAt:x.DeletedAt,
            //    AlertType = x.AndonAlertTypeId.ToString(),
            //    Comment = x.Comment,
            //    DeletedBy = x.DeletedByUser.HasValue() ? users.Where(y => y.Id == x.DeletedByUser).Select(y => (y.FirstName + " " + y.LastName).ToUpper()).FirstOrDefault() : "-",
            //    CreatedBy = users.Where(y => y.Id == x.UserId).Select(y => (y.FirstName + " " + y.LastName).ToUpper())
            //        .FirstOrDefault()
            //}).ToList();
            //    AndonReportsViewModel result = new AndonReportsViewModel
            //{
            //    AlertReportDetails = reportDetails,
            //    TotalLines = lines.Count
            //};
            //return result;
            throw new NotImplementedException();
        }

        public async Task<Byte[]> GenerateExcelReport(int factoryId, int processId, int weekId)
        {
            //var factory = await _factoryRepository.GetFactoryById(factoryId);
            //var process = await _processRepository.GetProcessById(processId);
            //var week = await _tegraEdwContext.DimWeekCalendar.FirstOrDefaultAsync(x => x.Id == weekId);
            //var reportDetails = await GetAlertListByProcessAndWeek(factoryId, processId, weekId);
            //var excel = new AndonExcelReportTemplate();
            //var fileContents = excel.GenerateExcelReport(factory, process, week, reportDetails);
            //return fileContents;
            throw new NotImplementedException();
        }

        Task<PagedList<AndonAlertReportDto>> IAndonReportService.GetAlertsPaginatedByProcessAndWeek(int factoryId, int processId, int weekId, int page, string sort, string search)
        {
            throw new NotImplementedException();
        }

        Task<AndonReportsViewModel> IAndonReportService.GetAlertListByProcessAndWeek(int factoryId, int processId, int weekId)
        {
            throw new NotImplementedException();
        }
    }
}
