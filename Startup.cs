using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using I3Teck.AndonAlarm.Data;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using InternalRequisition.Hubs;
using I3Teck.AndonAlarm.Data.Models;
using InternalRequisition.Services.Andon.Interfaces;
using InternalRequisition.Services.Andon;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using InternalRequisition.Repository;
using I3Teck.AndonAlarm.Repositories;
using InternalRequisition.Repository.Interfaces;
using I3Teck.AndonAlarm.Data.Repositories;
using I3Teck.AndonAlarm.Data.Models;
using Tegra.ProductDevelopment.Identity;

namespace I3Teck.AndonAlarm
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("AndonDb")));

            services.AddIdentity<User, Role>(options => { }).AddRoles<Role>()
                  .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders()
                  .AddSignInManager<SignInManager<User>>()
                  .AddUserManager<UserManager<User>>();

            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddSignalR();
            services.AddScoped<IUserClaimsPrincipalFactory<User>, ClaimsPrincipalFactory>();
            services.AddScoped<IAndonService, AndonService>();
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<IFactoryRepository, FactoryRepository>();
            services.AddScoped<IProcessRepository, ProcessRepository>();
            services.AddScoped<IAndonRepository, AndonRepository>();
            services.AddScoped<IAndonReasonAlertRepository, AndonReasonAlertRepository>();
            services.AddScoped<IShiftRepository, ShiftRepository>();
            services.AddScoped<IScheduleRepository, ScheduleRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseDatabaseErrorPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();
           
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<AndonHub>("/andon/alerts");
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });
        }
    }
}
