﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class AndonAlertReason
    {
        public int Id { get; set; }       
        public string Code { get; set; }        
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
