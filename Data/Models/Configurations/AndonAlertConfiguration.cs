﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models.Configurations
{
    public class AndonAlertConfiguration : IEntityTypeConfiguration<AndonAlert>
    {
        public void Configure(EntityTypeBuilder<AndonAlert> builder)
        {
            builder.ToTable("AndonAlerts");
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).UseIdentityColumn();
            builder.Property(a => a.Comment).HasMaxLength(200);
            builder.Property(a => a.CreatedAt).IsRequired();
            builder.HasOne(a => a.CreatedByUser).WithMany(u => u.AndonAlertsCreated).HasForeignKey(a => a.CreatedByUserId);
            builder.HasOne(a => a.DeletedByUser).WithMany(u => u.AndonAlertsDeleted).HasForeignKey(a => a.DeletedByUserId);
            builder.HasOne(a => a.ProductionLine).WithMany(p => p.AndonAlerts).HasForeignKey(a => a.ProductionLineId);
            builder.HasOne(a => a.AndonAlertType).WithMany(p => p.Alerts).HasForeignKey(a => a.AndonAlertTypeId);

        }
    }
}
