﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models.Configurations
{
    public class AndonAlertReasonConfiguration : IEntityTypeConfiguration<AndonAlertReason>
    {
        public void Configure(EntityTypeBuilder<AndonAlertReason> builder)
        {
            builder.ToTable("AndonAlertReasons");
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).UseIdentityColumn();
            builder.Property(a => a.Code).IsRequired().HasMaxLength(8);
            builder.Property(a => a.Name).IsRequired().HasMaxLength(50);
            builder.Property(a => a.Description).HasMaxLength(200);
        }
    }
}
