﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models.Configurations
{
    public class ScheduleConfiguration : IEntityTypeConfiguration<Schedule>
    {
        public void Configure(EntityTypeBuilder<Schedule> builder)
        {
            builder.ToTable("Schedules");
            builder.HasKey(s => s.Id);
            builder.Property(s => s.Id).UseIdentityColumn();
            builder.Property(s => s.DayOfWeek).IsRequired();
            builder.Property(s => s.StartTime).HasColumnType("Time").IsRequired();
            builder.Property(s => s.EndTime).HasColumnType("Time").IsRequired();
            builder.HasOne(s => s.Factory).WithMany(f => f.Schedules).HasForeignKey(f => f.FactoryId);
            builder.HasOne(s => s.Shift).WithMany(s => s.Schedules).HasForeignKey(s => s.ShiftId);
        }
    }
}
