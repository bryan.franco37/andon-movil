﻿using I3Teck.AndonAlarm.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models.Configurations
{
    public class UserProductionLineConfiguration : IEntityTypeConfiguration<UserProductionLine>
    {
        public void Configure(EntityTypeBuilder<UserProductionLine> builder)
        {
            builder.ToTable("UserProductionLines");
            builder.HasKey(u => u.Id);
            builder.Property(u => u.Id).UseIdentityColumn();
            builder.HasOne(u => u.ProductionLine).WithMany(p => p.UserProductionLines).HasForeignKey(u => u.ProductionLineId);
            builder.HasOne(u => u.User).WithMany(u => u.UserProductionLines).HasForeignKey(u => u.UserId);
        }
    }
}
