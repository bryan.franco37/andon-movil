﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models.Configurations
{
    public class AndonAlertTypeConfiguration : IEntityTypeConfiguration<AndonAlertType>
    {
        public void Configure(EntityTypeBuilder<AndonAlertType> builder)
        {
            builder.ToTable("AndonAlertTypes");
            builder.HasKey(a => a.Id);
            builder.Property(a => a.Id).UseIdentityColumn();
            builder.Property(a => a.Name).IsRequired().HasMaxLength(50);
            builder.Property(a => a.Color).IsRequired().HasMaxLength(50);
        }
    }
}
