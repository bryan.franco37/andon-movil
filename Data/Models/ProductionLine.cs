﻿using I3Teck.AndonAlarm.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class ProductionLine
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int BuildingId { get; set; }
        public Building Building { get; set; }

        public int ProcessId { get; set; }

        public Process Process { get; set; }

        public virtual IEnumerable<UserProductionLine> UserProductionLines { get; set; }

        public virtual IEnumerable<AndonAlert> AndonAlerts { get; set; }
    }
}
