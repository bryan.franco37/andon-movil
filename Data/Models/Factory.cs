﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;


namespace I3Teck.AndonAlarm.Data.Models
{
    public class Factory
    {
        public int Id { get; set; }       
        public string Name { get; set; }

        public virtual IEnumerable<Building> Buildings { get; set; }

        public virtual IEnumerable<Schedule> Schedules { get; set; }

    }
}
