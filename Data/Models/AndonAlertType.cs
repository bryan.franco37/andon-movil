﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class AndonAlertType
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public string Color { get; set; }
        public virtual ICollection<AndonAlert> Alerts { get; set; }
    }
}
