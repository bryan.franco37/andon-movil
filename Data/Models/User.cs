﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using I3Teck.AndonAlarm.Models;
using Microsoft.AspNetCore.Identity;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class User : IdentityUser
    {
        public User() : base()
        {
        }
        
        public string FirstName { get; set; }
                
        public string LastName { get; set; }

        public virtual IEnumerable<UserProductionLine>  UserProductionLines { get; set; }

        public virtual IEnumerable<AndonAlert> AndonAlertsCreated { get; set; }

        public virtual IEnumerable<AndonAlert> AndonAlertsDeleted { get; set; }


    }
}
