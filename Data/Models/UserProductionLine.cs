﻿using I3Teck.AndonAlarm.Data.Models;

namespace I3Teck.AndonAlarm.Models
{
    public class UserProductionLine
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public virtual User User { get; set; }
        public int ProductionLineId { get; set; }
        public ProductionLine ProductionLine { get; set; }

    }
}
