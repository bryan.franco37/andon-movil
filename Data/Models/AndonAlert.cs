﻿using System;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class AndonAlert
    {
        public int Id { get; set; }       
        public string Comment { get; set; }
        public DateTime CreatedAt { get; set; } = DateTime.UtcNow;
        public DateTime? DeletedAt { get; set; }                          
        public double? LeadTimeMinutes { get; set; }
        public double? RealLeadTimeMinutes { get; set; }
        public string CreatedByUserId { get; set; }
        public virtual User CreatedByUser { get; set; }
        public string DeletedByUserId { get; set; }
        public virtual User DeletedByUser { get; set; }
        public int ProductionLineId { get; set; }
        public ProductionLine ProductionLine { get; set; }
        public int AndonAlertTypeId { get; set; }
        public virtual AndonAlertType AndonAlertType { get; set; }
    }
}
