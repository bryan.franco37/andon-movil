﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class Schedule
    {
        public int Id { get; set; }

        public int DayOfWeek { get; set; }

        public TimeSpan StartTime { get; set; }
        
        public TimeSpan EndTime { get; set; }


        public int FactoryId { get; set; }
        public Factory Factory { get; set; }

        public int ShiftId { get; set; }

        public virtual Shift Shift { get; set; }       

    }
}
