﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class Building
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public int FactoryId { get; set; }

        public Factory Factory { get; set; }

        public virtual  IEnumerable<ProductionLine> ProductionLines { get; set; }
    }
}
