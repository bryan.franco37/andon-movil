﻿using Microsoft.AspNetCore.Identity;

namespace I3Teck.AndonAlarm.Data.Models
{
    public sealed class Role : IdentityRole
    {
        public Role() : base()
        {

        }

        public Role(string roleName)
        {
            Name = roleName;
            NormalizedName = roleName.ToUpper();
        }
    }
}
