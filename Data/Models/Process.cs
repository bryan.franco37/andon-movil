﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Data.Models
{
    public class Process
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<AndonAlert> Alerts { get; set; }

        public virtual ICollection<ProductionLine> ProductionLines { get; set; }
    }
}
