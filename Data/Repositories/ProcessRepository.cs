﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Enum;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace InternalRequisition.Repository
{
    public class ProcessRepository : IProcessRepository
    {
        private readonly ApplicationDbContext _mainContext;

        public ProcessRepository(ApplicationDbContext mainContext)
        {
            _mainContext = mainContext;
        }

        public async Task<Process> GetProcessById(int processId)
        {
            return await _mainContext.Processes.FirstOrDefaultAsync(x => x.Id == processId);
        }

        public async Task<Process> GetProcessByName(string processName)
        {
            return await _mainContext.Processes.Where(p => p.Name.Replace(" ", String.Empty).Equals(processName)).FirstOrDefaultAsync();
        }

        public async Task<List<Process>> GetAllProcesses()
        {
            return await _mainContext.Processes.ToListAsync();
        }

        public async Task<List<SelectListItem>> GetProcessListByUser(string userId)
        {
            var user = _mainContext.Users.FirstOrDefault(x => x.Id == userId);
            var userClaims = await _mainContext.UserClaims
                .Where(x => x.UserId == user.Id && x.ClaimType == nameof(UserClaimsTypeEnum.Planning))
                .Select(x => x.ClaimValue).ToListAsync();
            return await _mainContext.Processes.Where(x => userClaims.Contains(x.Name)).OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToListAsync();
        }
    }
}
