﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Dto;

namespace I3Teck.AndonAlarm.Repositories.Interfaces
{
    public interface IAndonRepository
    {
        Task<List<AndonAlert>> GetActiveAlerts(int factoryId, int processId);
        Task<List<AndonAlert>> GetAlertsByDateRange(int factoryId, int processId, DateTime? startDate, DateTime? endDate);
        Task<AndonAlert> GetAlertById(int andonAlertId, bool includeDeleted=false);
        Task<AndonAlert> GetAlertInfoById(int andonAlertId);

        Task<AndonAlert> GetAlertByPpmAddress(int ppmAddressId);
        Task<List<AndonAlert>> GetWarningAlerts();
        Task CreateAlertRange(List<AndonAlert> alert);
        void RemoveAlertRange(List<AndonAlert> alerts);
        Task<List<ProductionLineDto>> GetProductionLinesByProcessAndFactory(List<AndonAlert> alerts, int factoryId, int processId);
        Task<List<ProductionLineDto>> GetProductionLineByAlias(List<string> alias, List<AndonAlert> alerts, string prefix, string contactTitle, bool isSupervisor);
        Task<ProductionLineDto> GetProductionLineByPpmAddresId(List<AndonAlert> alerts, string prefix, string contactTitle, int ppmAddressId);
        Task<List<ProductionLineDto>> GetProductionLineByFactory(int factoryId);

        Task DeleteAndonAlert(int andonAlertId, string userId);

        Task SetLeadTimes(int andonAlertId, double leadTime, double realLeadTime);

        Task<List<AndonAlert>> GetClosedAndonAlerts();
        Task Commit();
    }
}