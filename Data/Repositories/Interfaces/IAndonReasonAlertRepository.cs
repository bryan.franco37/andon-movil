﻿using I3Teck.AndonAlarm.Data.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace InternalRequisition.Repository.Interfaces
{
    public interface IAndonReasonAlertRepository
    {
        Task<List<AndonAlertReason>> GetAll();
        Task<AndonAlertReason> GetById(int idAndonReasonAlert);
        Task<AndonAlertReason> GetByCode(int coodeAndonReasonAlert);
        Task<AndonAlertReason> Create(AndonAlertReason andonReasonAlert);
        Task<AndonAlertReason> Update(AndonAlertReason andonReasonAlert);
        Task<AndonAlertReason> Remove(AndonAlertReason andonReasonAlert);
    }
}
