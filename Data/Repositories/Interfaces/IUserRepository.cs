﻿using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;
using Microsoft.AspNetCore.Identity;

namespace I3Teck.AndonAlarm.Repositories.Interfaces
{
    public interface IUserRepository
    {
        Task<List<User>> GetUsers();
        Task<User> GetUserById(string userId);
        Task<User> GetUserByEmail(string email);
        Task<List<IdentityUserClaim<string>>> GetClaimsByUserId(string userId);
        Task<List<string>> GetUserAlias(string userId, int factoryId, int processId);
        Task<User> GetAndonCron();

    }
}
  