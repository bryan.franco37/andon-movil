﻿using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace I3Teck.AndonAlarm.Repositories.Interfaces
{
    public interface IProcessRepository
    {
        Task<Process> GetProcessById(int processId);
        Task<Process> GetProcessByName(string processName);
        Task<List<Process>> GetAllProcesses();
        Task<List<SelectListItem>> GetProcessListByUser(string userId);
    }
}
