﻿using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace I3Teck.AndonAlarm.Repositories.Interfaces
{
    public interface IFactoryRepository
    {
        Task<Factory> GetFactoryById(int factoryId);
        Task<Factory> GetFactoryByName(string factoryName);
        Task<List<Factory>> GetAllFactories();
        Task<List<Factory>> GetFactoriesbyUser(string userId);
        Task<List<SelectListItem>> GetFactoyListByUser(string userId);
    }
}
