﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;

namespace I3Teck.AndonAlarm.Repositories.Interfaces
{
  public interface IScheduleRepository
    {
        Task<List<Schedule>> GetScheduleByFactoryAsync(int factoryId, int shiftId);
        Task<Schedule> GetScheduleByDayAsync(int factory, int shiftId, int dayOfWeek);
        

    }
}
