﻿using I3Teck.AndonAlarm.Data.Models;
using System.Threading.Tasks;

namespace I3Teck.AndonAlarm.Repositories.Interfaces
{
  public  interface IShiftRepository
    {

        Task<Shift> GetShiftByIdAsync(int shiftId);
    }
}
