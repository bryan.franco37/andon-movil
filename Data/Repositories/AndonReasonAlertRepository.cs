﻿using System.Collections.Generic;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data.Models;
using InternalRequisition.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace I3Teck.AndonAlarm.Data.Repositories
{
    public class AndonReasonAlertRepository : IAndonReasonAlertRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;

        public AndonReasonAlertRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public async Task<List<AndonAlertReason>> GetAll()
        {
            return await _applicationDbContext.AndonAlertReasons.ToListAsync();
        }

        public async Task<AndonAlertReason> GetById(int idAndonReasonAlert)
        {
            return await _applicationDbContext.AndonAlertReasons.FirstOrDefaultAsync(x => x.Id == idAndonReasonAlert);
        }

        public async Task<AndonAlertReason> GetByCode(int coodeAndonReasonAlert)
        {
            return await _applicationDbContext.AndonAlertReasons.FirstOrDefaultAsync(x => x.Code.Equals(coodeAndonReasonAlert));
        }

        public async Task<AndonAlertReason> Create(AndonAlertReason andonReasonAlert)
        {
            _applicationDbContext.AndonAlertReasons.Add(andonReasonAlert);
            await _applicationDbContext.SaveChangesAsync();
            return andonReasonAlert;
        }

        public async Task<AndonAlertReason> Update(AndonAlertReason andonReasonAlert)
        {
            _applicationDbContext.AndonAlertReasons.Update(andonReasonAlert);
            await _applicationDbContext.SaveChangesAsync();
            return andonReasonAlert;
        }

        public async Task<AndonAlertReason> Remove(AndonAlertReason andonReasonAlert)
        {
            _applicationDbContext.AndonAlertReasons.Remove(andonReasonAlert);
            await _applicationDbContext.SaveChangesAsync();
            return andonReasonAlert;
        }
    }
}
