﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using InternalRequisition.Repository.Interfaces;
using Microsoft.EntityFrameworkCore;
using I3Teck.AndonAlarm.Dto;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Repositories.Interfaces;

namespace I3Teck.AndonAlarm.Repositories
{
    public class AndonRepository : IAndonRepository
    {
        private readonly ApplicationDbContext _mainContext;


        public AndonRepository(ApplicationDbContext mainContext)
        {
            _mainContext = mainContext;
        }

        public async Task<List<AndonAlert>> GetActiveAlerts(int factoryId, int processId)
        {
            var alerts = await _mainContext.AndonAlerts
                .Where(x => !x.DeletedAt.HasValue)
                .AsNoTracking().ToListAsync();

            return alerts.Select(x =>
            {
                x.CreatedAt = (x.CreatedAt.Kind == DateTimeKind.Utc || x.CreatedAt.Kind == DateTimeKind.Unspecified)
                    ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.CreatedAt, TimeZoneInfo.Local)
                    : x.CreatedAt;
                x.DeletedAt = (x.DeletedAt != null
                    ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.DeletedAt, TimeZoneInfo.Local)
                    : x.DeletedAt);
                return x;
            }).ToList();
        }

        public async Task<List<AndonAlert>> GetWarningAlerts()
        {
            return await _mainContext.AndonAlerts.Where(x => !x.DeletedAt.HasValue && x.AndonAlertTypeId==2).ToListAsync();
        }
        
        public async Task<List<AndonAlert>> GetAlertsByDateRange(int factoryId, int processId, DateTime? startDate,
            DateTime? endDate)
        {
            var alerts = await _mainContext.AndonAlerts.Where(x => endDate != null && (startDate != null &&
                                                                                       (x.CreatedAt >=
                                                                                        startDate.Value &&
                                                                                        x.CreatedAt <= endDate.Value 
                                                                                        //&&
                                                                                        //x.FactoryId == factoryId &&
                                                                                        //x.ProcessId == processId
                                                                                        )))
                .ToListAsync();

            return alerts.Select(x =>
            {
                x.CreatedAt = TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.CreatedAt, TimeZoneInfo.Local);
                x.DeletedAt = (x.DeletedAt != null
                    ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.DeletedAt, TimeZoneInfo.Local)
                    : x.DeletedAt);
                return x;
            }).ToList(); 
        }

        public async Task<AndonAlert> GetAlertById(int andonAlertId, bool includeDeleted=false)
        {
            AndonAlert x;
            if (!includeDeleted)
             x = await _mainContext.AndonAlerts.FirstOrDefaultAsync(a => a.Id == andonAlertId && !a.DeletedAt.HasValue);
            else
             x = await _mainContext.AndonAlerts.FirstOrDefaultAsync(a => a.Id == andonAlertId);

            return x;
        }

        public async Task<AndonAlert> GetAlertInfoById(int andonAlertId)
        {
            var x = await _mainContext.AndonAlerts.Include("ProductionLine").Include("ProductionLine.Building").AsNoTracking().FirstOrDefaultAsync(a =>
                a.Id == andonAlertId);

            x.CreatedAt = (x.CreatedAt.Kind == DateTimeKind.Utc || x.CreatedAt.Kind == DateTimeKind.Unspecified)
                ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.CreatedAt, TimeZoneInfo.Local)
                : x.CreatedAt;
            x.DeletedAt = (x.DeletedAt != null &&(x.CreatedAt.Kind == DateTimeKind.Utc || x.CreatedAt.Kind == DateTimeKind.Unspecified)
                ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.DeletedAt, TimeZoneInfo.Local)
                : x.DeletedAt);

            return x;
        }
        
        public async Task<AndonAlert> GetAlertByPpmAddress(int ppmAddressId)
        {
            return await _mainContext.AndonAlerts.FirstOrDefaultAsync(x =>
                x.ProductionLineId == ppmAddressId && !x.DeletedAt.HasValue);
        }

        public async  Task CreateAlertRange(List<AndonAlert> alerts)
        {
            await _mainContext.AndonAlerts.AddRangeAsync(alerts); 
        }

        public  void RemoveAlertRange(List<AndonAlert> alerts)
        {
             _mainContext.UpdateRange(alerts);
        }

        public async Task<List<ProductionLineDto>> GetProductionLinesByProcessAndFactory(List<AndonAlert> alerts,
            int factoryId, int processId)
        {
            var productionLine = await _mainContext.ProductionLines.Include(x => x.Building)
                .Where(x => x.Building.FactoryId == factoryId && x.ProcessId == processId).ToListAsync();
            return (productionLine).Select(x =>
                new ProductionLineDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Alert = alerts.FirstOrDefault(y => y.ProductionLineId== x.Id),
                    Building = x.Building.Name
                }).OrderBy(x => x.Building + x.Name).ToList();
        }

        public async Task<List<ProductionLineDto>> GetProductionLineByAlias(List<string> alias, List<AndonAlert> alerts,
            string prefix, string contactTitle, bool isSupervisor)
        {
            //var addreses = await _ppmDbContext.GetProductionLine(alias, prefix, contactTitle, isSupervisor);
            //return addreses.Select(x => new ProductionLineDto
            //{
            //    Id = x.AddressId,
            //    Name = x.CompanyNumber,
            //    Alert = alerts.FirstOrDefault(y => y.Id == x.AddressId),
            //    Building = (x.Comments is null) ? "<Empty>" : x.Comments
            //}).OrderBy(x => x.Building + x.Name).ToList();
            throw new NotImplementedException(); 
        }

        public async Task<ProductionLineDto> GetProductionLineByPpmAddresId(List<AndonAlert> alerts, string prefix,
            string contactTitle, int ppmAddressId)
        {
            //return (await _ppmDbContext.GetProductionLineByPpmAddressId(prefix, contactTitle, ppmAddressId)).Select(x =>
            //    new ProductionLineDto
            //    {
            //        Id = x.AddressId,
            //        Name = x.CompanyNumber,
            //        Alert = alerts.FirstOrDefault(y => y.PpmAddressId == x.AddressId),
            //        Building = (x.Comments is null) ? "<Empty>" : x.Comments
            //    }).OrderBy(x => x.Building + x.Name).FirstOrDefault();

            throw new NotImplementedException();
        }

        public async Task<List<ProductionLineDto>> GetProductionLineByFactory(int factoryId)
        {

            return _mainContext.ProductionLines.Where(x=> x.Building.FactoryId == factoryId).Select(x =>
                new ProductionLineDto
                {
                    Id = x.Id,
                    Name = x.Name,
                    Building = x.Building.Name
                }).OrderBy(x => x.Building + x.Name).ToList();
        }

      
        public async Task Commit()
        {
            await _mainContext.SaveChangesAsync();
        }
        
        public async Task DeleteAndonAlert(int andonAlertId, string userId)
        {
            var alert = await GetAlertById(andonAlertId);
            alert.DeletedAt = DateTime.UtcNow;
            alert.DeletedByUserId = userId;
        }

        public async Task SetLeadTimes(int andonAlertId, double leadTime, double realLeadTime)
        {
            var alert = await GetAlertById(andonAlertId, true);
            alert.LeadTimeMinutes = leadTime;
            alert.RealLeadTimeMinutes = (realLeadTime<0)?0:realLeadTime;

        }

        public async Task<List<AndonAlert>> GetClosedAndonAlerts()
        {
            return await _mainContext.AndonAlerts.AsNoTracking().Where(a => a.DeletedAt.HasValue && !a.LeadTimeMinutes.HasValue).Take(500)
                .Select(x=>new AndonAlert { 
                    Id= x.Id, 
                    CreatedAt= (x.CreatedAt.Kind == DateTimeKind.Utc || x.CreatedAt.Kind == DateTimeKind.Unspecified)
                        ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.CreatedAt, TimeZoneInfo.Local) : x.CreatedAt,
                    DeletedAt = (x.DeletedAt != null && (x.CreatedAt.Kind == DateTimeKind.Utc || x.CreatedAt.Kind == DateTimeKind.Unspecified))
                        ? TimeZoneInfo.ConvertTimeFromUtc((DateTime)x.DeletedAt, TimeZoneInfo.Local) : x.DeletedAt//, 
                    //FactoryId=x.FactoryId
             }).ToListAsync();

        }


    }
}