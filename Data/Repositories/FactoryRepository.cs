﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Enum;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace I3Teck.AndonAlarm.Repositories
{
    public class FactoryRepository : IFactoryRepository
    {
        private readonly ApplicationDbContext _mainContext;

        public FactoryRepository(ApplicationDbContext mainContext)
        {
            _mainContext = mainContext;
        }

        public async Task<Factory> GetFactoryById(int factoryId)
        {
            return await _mainContext.Factories.FirstOrDefaultAsync(x => x.Id == factoryId);
        }

        public async Task<Factory> GetFactoryByName(string factoryName)
        {
            return  await _mainContext.Factories.FirstOrDefaultAsync(x=>x.Name.Replace(" " , string.Empty).ToLower().Equals(factoryName.Replace(" ", string.Empty).ToLower()));
        }

        public async Task<List<Factory>> GetAllFactories()
        {
            return await _mainContext.Factories.ToListAsync();
        }

        public async Task<List<Factory>> GetFactoriesbyUser(string userId)
        {
            var user = _mainContext.Users.FirstOrDefault(x => x.Id == userId);
            var userClaims = await _mainContext.UserClaims
                .Where(x => x.UserId == user.Id && x.ClaimType == nameof(UserClaimsTypeEnum.FactoryId))
                .Select(x => Convert.ToInt32(x.ClaimValue)).ToListAsync();
            return await _mainContext.Factories.Where(x => userClaims.Contains(x.Id))
                .OrderBy(x => x.Name).ToListAsync();
        }

        public async Task<List<SelectListItem>> GetFactoyListByUser(string userId)
        {
            var user = _mainContext.Users.FirstOrDefault(x => x.Id == userId);
            var userClaims = await _mainContext.UserClaims
                .Where(x => x.UserId == user.Id && x.ClaimType == nameof(UserClaimsTypeEnum.FactoryId))
                .Select(x => Convert.ToInt32(x.ClaimValue)).ToListAsync();
            return await _mainContext.Factories.Where(x => userClaims.Contains(x.Id))
                .OrderBy(x => x.Name)
                .Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name }).ToListAsync();
        }
    }
}