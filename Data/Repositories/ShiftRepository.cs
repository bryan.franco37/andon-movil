﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace InternalRequisition.Repository
{
    public class ShiftRepository  : IShiftRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly ILogger<ShiftRepository> _logger;

        public ShiftRepository(ApplicationDbContext context, ILogger<ShiftRepository> logger)
        {
            _context = context;
            _logger = logger;
        }


        public async Task<Shift> GetShiftByIdAsync(int shiftId)
        {
            try
            {
                var shift = await _context.Shift.Where(s => s.Id == shiftId).FirstOrDefaultAsync();
                return shift;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error getting Shift {ex.Message}");
                return null;
            }
        }
    }
}
