﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

namespace InternalRequisition.Repository
{
    public class UserRepository: IUserRepository
    {
        private readonly ApplicationDbContext _mainContext;

        public UserRepository(ApplicationDbContext mainContext)
        {
            _mainContext = mainContext;
        }
        
        public async Task<List<IdentityUserClaim<string>>> GetClaimsByUserId(string userId)
        {
           var roles =_mainContext.UserRoles.Where(x => x.UserId == userId);
           return await _mainContext.UserClaims.Where(x => x.UserId == userId).ToListAsync();
        }

        public async Task<List<User>> GetUsers()
        {
           return await _mainContext.Users.ToListAsync();
        }

        public async Task<User> GetUserById(string userId)
        {
            return await _mainContext.Users.FirstOrDefaultAsync(x => x.Id== userId);
        }

        public async Task<User> GetUserByEmail(string email)
        {
            return await _mainContext.Users.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<List<string>> GetUserAlias(string userId, int factoryId, int processId)
        {
            return await _mainContext.UserProductionLines.Where(x => x.UserId == userId && x.ProductionLine.Building.FactoryId == factoryId && x.ProductionLine.ProcessId == processId).Select(x => x.ProductionLineId.ToString()).ToListAsync();
        }

        public async Task<User> GetAndonCron()
        {
            return await _mainContext.Users.Where(x => x.FirstName == "andon" && x.LastName == "cron").FirstOrDefaultAsync();
        }
    }
}
