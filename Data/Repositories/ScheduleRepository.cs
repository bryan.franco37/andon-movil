﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using I3Teck.AndonAlarm.Data;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace I3Teck.AndonAlarm.Repositories
{
    public class ScheduleRepository : IScheduleRepository
    {
        private readonly ApplicationDbContext _mainContext;
        private readonly ILogger<ScheduleRepository> _logger;

        public ScheduleRepository(ApplicationDbContext mainContext, ILogger<ScheduleRepository> logger)
        {
            _mainContext = mainContext;
            _logger = logger;
        }

        
        public async  Task<List<Schedule>> GetScheduleByFactoryAsync(int factoryId, int shiftId)
        {
            List<Schedule> lst = new List<Schedule>();
            try
            {
              lst= await _mainContext.Schedule.Where(s => s.FactoryId == factoryId && s.ShiftId == shiftId).ToListAsync();
            }
            catch (Exception ex)
            {
             _logger.LogError($"Error getting schedule by factory {ex.Message}");   
            }
            return lst;
        }

        public async Task<Schedule> GetScheduleByDayAsync(int factoryId, int shiftId, int dayOfWeek)
        {
            try
            {
                var schedule = await _mainContext.Schedule
                    .Where(s => s.FactoryId == factoryId && s.ShiftId == shiftId && s.DayOfWeek == dayOfWeek)
                    .FirstOrDefaultAsync();
                return schedule;
            }
            catch (Exception ex)
            {
                _logger.LogError($"Error getting Schedule {ex.Message}");
                return null;
            }
        }
    }
}
