﻿using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Data.Models.Configurations;
using I3Teck.AndonAlarm.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace I3Teck.AndonAlarm.Data
{
    public class ApplicationDbContext :IdentityDbContext<User, Role, string>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
            
        }
        public DbSet<AndonAlert> AndonAlerts { get; set; }
        public DbSet<AndonAlertReason> AndonAlertReasons { get; set; }
        public DbSet<AndonAlertType> AndonAlertTypes { get; set; }    
        public DbSet<Building> Buildings { get; set; }
        public DbSet<Factory> Factories { get; set; }
        public DbSet<Process> Processes { get; set; }
        public DbSet<ProductionLine> ProductionLines { get; set; }    
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<Shift> Shift { get; set; }       
        public DbSet<UserProductionLine> UserProductionLines { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration<AndonAlert>(new AndonAlertConfiguration());
            builder.ApplyConfiguration<AndonAlertReason>(new AndonAlertReasonConfiguration());
            builder.ApplyConfiguration<AndonAlertType>(new AndonAlertTypeConfiguration());
            builder.ApplyConfiguration<Building>(new BuildingConfiguration());
            builder.ApplyConfiguration<Factory>(new FactoryConfiguration());
            builder.ApplyConfiguration<Process>(new ProcessConfiguration());
            builder.ApplyConfiguration<ProductionLine>(new ProductionLineConfiguration());
            builder.ApplyConfiguration<Schedule>(new ScheduleConfiguration());
            builder.ApplyConfiguration<Shift>(new ShiftConfiguration());
            builder.ApplyConfiguration<UserProductionLine>(new UserProductionLineConfiguration());
            base.OnModelCreating(builder);
        }
    }
}
