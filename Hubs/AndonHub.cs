﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;

namespace InternalRequisition.Hubs
{
    public class AndonHub: Hub
    {
        public override Task OnConnectedAsync()
        {
            return base.OnConnectedAsync();
        }

        public override Task OnDisconnectedAsync(Exception exception)
        {
         
            return base.OnDisconnectedAsync(exception);
        }

        public Task AddToGroup(string groupName) {
           return Groups.AddToGroupAsync(Context.ConnectionId, groupName);          
        }


    }
}
