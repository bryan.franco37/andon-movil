﻿using System.Linq;
using System.Security.Claims;
using System.Security.Principal;

namespace I3Teck.AndonAlarm.Extensions
{
    public static class IdentityExtensions
    {
        public static string GetFirstName(this IIdentity identity)
        {
            return GetValueOrDefault(identity, ClaimTypes.GivenName);
        }

        public static string GetLastName(this IIdentity identity)
        {
            return GetValueOrDefault(identity, ClaimTypes.Surname);
        }

        public static string GetFullNameOrEmail(this IIdentity identity)
        {
            var firstName = identity.GetFirstName();
            var lastName = identity.GetLastName();

            return string.IsNullOrEmpty(firstName) && string.IsNullOrEmpty(lastName) ? GetValueOrDefault(identity, ClaimTypes.Name) : $"{firstName} {lastName}";
        }

        private static string GetValueOrDefault(IIdentity identity, string claimType)
        {
            var claim = ((ClaimsIdentity)identity).FindFirst(claimType);

            return claim == null ? string.Empty : claim.Value;
        }

        public static string GetInitials(this IIdentity identity)
        {
            var claims = identity as ClaimsIdentity;

            return $"{claims.FindFirst(ClaimTypes.GivenName)?.Value?.ToUpper().ToCharArray().FirstOrDefault() ?? ' '}{claims.FindFirst(ClaimTypes.Surname)?.Value?.ToUpper().ToCharArray().FirstOrDefault() ?? ' '}";
        }

        public static string GetId(this IIdentity identity)
        {
            return (identity as ClaimsIdentity).FindFirst("TegraId").Value;
        }


    }
}
