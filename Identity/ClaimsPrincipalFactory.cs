﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using I3Teck.AndonAlarm.Data.Models;
using I3Teck.AndonAlarm.Data;

namespace Tegra.ProductDevelopment.Identity
{
    public class ClaimsPrincipalFactory : UserClaimsPrincipalFactory<User,Role>
    {
        private readonly ApplicationDbContext _context;
        public ClaimsPrincipalFactory(UserManager<User> userManager, RoleManager<Role> roleManager, IOptions<IdentityOptions> options,ApplicationDbContext context) : 
            base(userManager, roleManager, options)
        {
            _context = context;
        }

        public override  async Task<ClaimsPrincipal> CreateAsync(User user)
        {
            var principal = await base.CreateAsync(user);
            var claimIdentity = ((ClaimsIdentity)principal.Identity);
            var customClaims = new List<Claim>();
            // var claims = UserManager.GetClaimsAsync(user);
            if (RoleManager.SupportsRoleClaims)
            {
                var roles = ((ClaimsIdentity) principal.Identity).FindAll(ClaimTypes.Role).Select(x => x.Value.ToUpper());
                var roleClaims = await (from role in _context.Roles
                    join claims in _context.RoleClaims on role.Id equals claims.RoleId
                    where roles.Contains(role.NormalizedName)
                    select claims).ToListAsync();
                customClaims.AddRange(roleClaims.Select(x => new Claim(x.ClaimType, x.ClaimValue)));


            }
            customClaims.AddRange(new[] {
                new Claim(ClaimTypes.GivenName, user.FirstName??string.Empty),
                new Claim(ClaimTypes.Surname, user.LastName??string.Empty),
                new Claim("TegraId", user.Id??string.Empty),
            });

            claimIdentity.AddClaims(customClaims);
            return principal;
        }

    }
}
